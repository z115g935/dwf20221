export class ApiURI{
    public static exchangeRateURI: string = "https://v6.exchangerate-api.com/v6/2a24b6b03011ca8a7a2281e1/latest/{rate}";
    public static dwf20221apiURI: string = "http://localhost:8080";
}