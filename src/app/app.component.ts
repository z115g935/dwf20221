import { Component } from '@angular/core';
import { CroppedEvent } from 'ngx-photo-editor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'dwfmarket';
  imageChangedEvent: any;
  base64: any;

  fileChangedEvent(event: any){
    this.imageChangedEvent = event;
  }

  imageCropped(event: CroppedEvent){
    this.base64 = event.base64;
  }
}
