import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/modules/product/_model/product';
import { Category } from 'src/app/modules/product/_model/category';
import { ProductImage } from 'src/app/modules/product/_model/productImage';
import { ProductService } from 'src/app/modules/product/_service/product.service';
import { CategoryService } from 'src/app/modules/product/_service/category.service';
import { ProductImageService } from 'src/app/modules/product/_service/product-image.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  products: Product[] = [];
  product: Product = new Product();
  categories: Category[] = [];
  category: Category = new Category();
  images: ProductImage[] = [];
  image: ProductImage = new ProductImage();

  constructor(
    private product_service: ProductService,
    private category_service: CategoryService,
    private product_image_service: ProductImageService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getProductsRandom();
    this.getCategories();
  }

  getProducts(){
    this.product_service.getProducts().subscribe(
      res => {
        this.products = res
      },
      err => console.log(err)
    )
  }

  getCategories(){
    this.category_service.getCategories().subscribe(
      res => {
        this.categories = res;
        console.log(this.categories);
      },
      err => console.log(err)
    )
  }

  getCategory(id_category: number){
    this.category_service.getCategory(id_category).subscribe(
      res => {
        this.category = res;
        console.log(this.categories);
      },
      err => console.log(err)
    )
  }

  getProductByCategory(id_categoria: number){
    this.product_service.getProductByCategory(id_categoria).subscribe(
      res => {
        console.log(res);
        this.products = res;
      },
      err => console.log(err)
    )
  }

  getProductsRandom(){
    this.product_service.getProductsRandom().subscribe(
      res => {
        console.log(res);
        this.products = res;
      },
      err => console.log(err)
    )
  }

  getProductImages(id_product: number,){
    this.product_image_service.getProductImages(id_product).subscribe(
      res => {
        this.images = res;
        console.log("imagenes:" + this.images);
      },
      err => console.log(err)
    )
  }

  productDetail(gtin: string){
    this.router.navigate(['product-detail/' + gtin]);
  }

}
