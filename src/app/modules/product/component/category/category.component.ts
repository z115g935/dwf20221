import { Component, OnInit } from '@angular/core';
import { Category } from '../../_model/category';
import { CategoryService } from '../../_service/category.service';
import { FormBuilder, Validators } from '@angular/forms';

declare var $: any;

import Swal from 'sweetalert2';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categories: Category[] = [];
  category: Category = new Category();
  formulario = this.formBuilder.group({
    id_category: [''],
    category: ['', Validators.required]
  });
  post_category = false;
  submitted = false;

  constructor(
    private category_service: CategoryService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories(){
    this.category_service.getCategories().subscribe(
      res => {
        this.categories = res;
        console.log(this.categories);
      },
      err => console.log(err)
    )
  }

  getCategory(id_category: number){
    this.category_service.getCategory(id_category).subscribe(
      res => {
        this.category = res;
        console.log(this.categories);
      },
      err => console.log(err)
    )
  }

  onSubmit(){
    this.submitted = true;
    if(this.post_category){
      this.category_service.createCategory(this.formulario.value).subscribe(
        res => {
          console.log(this.categories);
          this.getCategories();
          this.closeModal();
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'La categoria ha sido registrada',
            showConfirmButton: false,
            timer: 2000
          })
        },
        err => {
          console.log(err);
          Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'No se puede agregar esta categoria',
            showConfirmButton: false,
            timer: 2000
          })
        }
      )
    }else{
      this.category_service.updateCategory(this.formulario.value).subscribe(
        res => {
          console.log(this.categories);
          this.getCategories();
          this.closeModal();
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'La categoria ha sido actualizada',
            showConfirmButton: false,
            timer: 2000
          })
        },
        err => {
          console.log(err);
          Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'No se puede actualizar esta categoria',
            showConfirmButton: false,
            timer: 2000
          })
        }
      )
    }
  }

  createCategory(){
    this.post_category = true;
    this.formulario.reset();
    $("#category_modal_create").modal("show");
  }

  updateCategory(category: Category){
    this.post_category = false;
    this.formulario.controls['id_category'].setValue(category.id_category);
    this.formulario.controls['category'].setValue(category.category);
    $("#category_modal_update").modal("show");
  }

  deleteCategory(id_category: number){
    Swal.fire({
      title: 'Esta seguro de eliminar la categoria?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.category_service.deleteCategory(id_category).subscribe(
          res => {
            console.log(this.categories);
            this.getCategories();
          },
          err => {
            console.log(err);
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: 'La categoria seleccionada no puede ser eliminada',
              showConfirmButton: false,
              timer: 2000
            })
          }
        )
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'La categoria ha sido eliminada',
          showConfirmButton: false,
          timer: 2000
        })
      }
    })
  }

  get f(){
    return this.formulario.controls;
  }

  closeModal(){
    $("#category_modal_create").modal("hide");
    $("#category_modal_update").modal("hide");
    this.submitted = false;
  }

}
