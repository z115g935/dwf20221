import { Component, OnInit } from '@angular/core';
import { Product } from '../../_model/product';
import { Category } from '../../_model/category';
import { ProductService } from '../../_service/product.service';
import { CategoryService } from '../../_service/category.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

declare var $: any;

import Swal from 'sweetalert2';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  products: Product[] = [];
  categories: Category[] = [];
  formulario = this.formBuilder.group({
    id_product: [''],
    gtin: ['', Validators.required],
    product: ['', Validators.required],
    description: [''],
    price: ['', Validators.required],
    stock: ['', Validators.required],
    id_category: ['', Validators.required],
    status: [''],
  });
  submitted = false;

  constructor(
    private product_service: ProductService,
    private category_service: CategoryService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts(){
    this.product_service.getProducts().subscribe(
      res => {
        this.products = res
      },
      err => console.log(err)
    )
  }

  onSubmit(){
    this.submitted = true;
    if(this.formulario.invalid){
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Faltan campos obligatorios po llenar',
        showConfirmButton: false,
        timer: 2000
      })
      return;
    }
    this.product_service.createProduct(this.formulario.value).subscribe(
      res => {
        this.getProducts();
        this.closeModal();
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Registro exitoso',
          showConfirmButton: false,
          timer: 2000
        })
      },
      err =>{
        console.log(err);
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'No se pudo registrar el producto',
          showConfirmButton: false,
          timer: 2000
        })
      }
    )
  }

  createProduct(){
    this.getCategories();
    this.formulario.reset();
    this.formulario.controls['id_category'].setValue(0);
    $("#product_modal").modal("show");
  }
  
  get f(){
    return this.formulario.controls;
  }

  closeModal(){
    $("#product_modal").modal("hide");
    this.submitted = false;
  }

  deleteProduct(id_product: number){
    Swal.fire({
      position: 'top-end',
      icon: 'warning',
      title: 'Desea eliminar el producto?',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Eliminar!'
    }).then((result) => {
      if(result.isConfirmed){
        this.product_service.deleteProduct(id_product).subscribe(
          res => {
            this.getProducts();
            Swal.fire({
              position: 'top-end',
              icon: 'success',
              title: 'Eliminacion exitosa',
              showConfirmButton: false,
              timer: 2000
            })
          },
          err => {
            console.log(err);
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: 'El producto no se puede eliminar',
              showConfirmButton: false,
              timer: 2000
            })
          }
        )
      }
    })
  }

  getCategories(){
    this.category_service.getCategories().subscribe(
      res => {
        this.categories = res;
      },
      err => console.log(err)
    )
  }

  productDetail(gtin: string){
    this.router.navigate(['product-detail/' + gtin]);
  }

}
