import { Component, OnInit } from '@angular/core';
import { Cart } from '../../_model/cart';
import { Factura } from '../../_model/factura';
import { CartService } from '../../_service/cart.service';
import { FacturaService } from '../../_service/factura.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

declare var $: any;

import Swal from 'sweetalert2';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cart: Cart[] = [];
  id_product: number = 0;
  quantity: number = 0;
  id_invoice: number = 0;
  rfc: string = "SIDA920101A00";
  facrtura: Factura[] = [];
  formulario = this.formBuilder.group({
    tdc: [0, Validators.required],
    expiracion: [0, Validators.required],
    numSeguridad: [0, Validators.required]
  });
  submitted = false;

  constructor(
    private cart_service: CartService,
    private factura_service: FacturaService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getCart(this.rfc);
  }

  addToCart(cart: Cart){
    this.cart_service.addToCart(this.rfc, this.quantity, this.id_product).subscribe(
      res => {
        console.log(res);
        this.getCart(this.rfc);
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Se agrego al carrito exitosamente',
          showConfirmButton: false,
          timer: 2000
        })
      },
      err => {
        console.log(err);
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'No se pudo agregar al carrito',
          showConfirmButton: false,
          timer: 2000
        })
      }
    )
  }

  removeFromCart(id_cart: number){
    this.cart_service.removeFromCart(id_cart).subscribe(
      res => {
        console.log(res);
        this.getCart(this.rfc);
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Se elimino el producto',
          showConfirmButton: false,
          timer: 2000
        })
      },
      err => {
        console.log(err);
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'No se pudo eliminar el producto',
          showConfirmButton: false,
          timer: 2000
        })
      }
    )
  }

  getCart(rfc: string){
    this.cart_service.getCart(rfc).subscribe(
      res => {
        console.log(res);
        this.cart = res;
      },
      err => console.log(err)
    )
  }

  deleteCart(rfc: string){
    this.cart_service.deleteCart(rfc).subscribe(
      res => {
        console.log(res);
        this.getCart(this.rfc);
      },
      err => {
        console.log(err);
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'No se pudo eliminar el carrito',
          showConfirmButton: false,
          timer: 2000
        })
      }
    )
  }

  purchase(rfc: string){
    this.submitted = true;
    this.factura_service.purchase(rfc).subscribe(
      res => {
        console.log(res);
        //this.deleteCart(rfc);
        this.getCart(this.rfc);
        this.closeModal();
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Compra realizada con exito',
          showConfirmButton: false,
          timer: 2000
        })
      },
      err => {
        console.log(err);
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'No se pudo realizar la compra',
          showConfirmButton: false,
          timer: 2000
        })
      }
    )
  }

  comprar(){
    this.formulario.reset();
    //this.formulario.controls['id_invoice'].setValue(0);
    $("#factura_modal").modal("show");
  }

  get f(){
    return this.formulario.controls;
  }

  closeModal(){
    $("#factura_modal").modal("hide");
    this.submitted = false;
  }

}
