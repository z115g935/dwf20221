import { Component, OnInit } from '@angular/core';
import { Factura } from '../../_model/factura';
import { Item } from '../../_model/item';
import { FacturaService } from '../../_service/factura.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-factura-detail',
  templateUrl: './factura-detail.component.html',
  styleUrls: ['./factura-detail.component.css']
})
export class FacturaDetailComponent implements OnInit {

  factura: Factura = new Factura();
  facturas: Factura[] = [];
  item: Item[] = [];
  rfc: string = "SIDA920101A00";
  id_invoice: any = null;

  constructor(
    private factura_service: FacturaService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.id_invoice = this.route.snapshot.paramMap.get('id_invoice');
    this.getCart(this.rfc);
    this.getItems(this.id_invoice);
  }

  getCart(rfc: string){
    this.factura_service.getCart(rfc).subscribe(
      res => {
        console.log(res);
        this.facturas = res;
      },
      err => console.log(err)
    )
  }

  getItems(id_invoice: number){
    this.factura_service.getItems(id_invoice).subscribe(
      res => {
        console.log(res);
        this.item = res;
      },
      err => console.log(err)
    )
  }

}
