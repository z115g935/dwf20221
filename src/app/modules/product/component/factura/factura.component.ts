import { Component, OnInit } from '@angular/core';
import { Factura } from '../../_model/factura';
import { FacturaService } from '../../_service/factura.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-factura',
  templateUrl: './factura.component.html',
  styleUrls: ['./factura.component.css']
})
export class FacturaComponent implements OnInit {

  factura: Factura[] = [];
  rfc: string = "SIDA920101A00";

  constructor(
    private factura_service: FacturaService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getCart(this.rfc);
  }

  getCart(rfc: string){
    this.factura_service.getCart(rfc).subscribe(
      res => {
        console.log(res);
        this.factura = res;
      },
      err => console.log(err)
    )
  }

  getItems(id_invoice: number){
    this.factura_service.getItems(id_invoice).subscribe(
      res => {
        console.log(res);
      },
      err => console.log(err)
    )
  }

  facturaDetail(id_invoice: number){
    this.router.navigate(['/invoice/item/' + id_invoice]);
  }

}
