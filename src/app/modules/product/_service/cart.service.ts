import { Injectable } from '@angular/core';
import { Cart } from '../_model/cart';
import { HttpClient } from '@angular/common/http';
import { ApiURI } from 'src/app/shared/apis-uri';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private apiURI = ApiURI.dwf20221apiURI;
  private resource = "/cart";

  constructor(
    private http: HttpClient
  ) { }

  addToCart(rfc: string, quantity: number, id_product: number){
    return this.http.post(this.apiURI + this.resource, {rfc: rfc, quantity: quantity, id_product: id_product});
  }

  removeFromCart(id_cart: number){
    return this.http.delete(this.apiURI + this.resource + `/${id_cart}`);
  }

  getCart(rfc: string){
    return this.http.get<Cart[]>(this.apiURI + this.resource + `/${rfc}`);
  }

  deleteCart(rfc: string){
    return this.http.delete(this.apiURI + this.resource + "/clear" +`/${rfc}`)
  }
}
