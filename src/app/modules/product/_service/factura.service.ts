import { Injectable } from '@angular/core';
import { Factura } from '../_model/factura';
import { Item } from '../_model/item';
import { HttpClient } from '@angular/common/http';
import { ApiURI } from 'src/app/shared/apis-uri';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  private apiURI = ApiURI.dwf20221apiURI;
  private resource = "/invoice";

  constructor(
    private http: HttpClient
  ) { }
  
    getCart(rfc: string){
      return this.http.get<Factura[]>(this.apiURI + this.resource + `/${rfc}`);
    }

    purchase(rfc: string){
      return this.http.post(this.apiURI + this.resource + `/${rfc}`, null);
    }

    getItems(id_invoice: number){
      return this.http.get<Item[]>(this.apiURI + this.resource + "/item" + `/${id_invoice}`);
    }

}
