import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryComponent } from './component/category/category.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductComponent } from './component/product/product.component';
import { ProductDetailComponent } from './component/product-detail/product-detail.component';
import { NgxPhotoEditorModule } from 'ngx-photo-editor';
import { CartComponent } from './component/cart/cart.component';
import { FacturaComponent } from './component/factura/factura.component';
import { FacturaDetailComponent } from './component/factura-detail/factura-detail.component';

@NgModule({
  declarations: [
    CategoryComponent,
    ProductComponent,
    ProductDetailComponent,
    CartComponent,
    FacturaComponent,
    FacturaDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPhotoEditorModule
  ],
  exports: [
    CategoryComponent,
    ProductComponent,
    ProductDetailComponent,
    CartComponent,
    FacturaComponent,
    FacturaDetailComponent
  ]
})
export class ProductModule { }
