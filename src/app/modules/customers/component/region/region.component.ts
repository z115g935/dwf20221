import { Component, OnInit } from '@angular/core';
import { Region } from '../../_model/region';
import { RegionService } from '../../_service/region.service';
import { FormBuilder, Validators } from '@angular/forms';

declare var $: any;

import Swal from 'sweetalert2';

@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.css']
})
export class RegionComponent implements OnInit {

  regions: Region[] = [];
  region: Region = new Region();
  formulario = this.formBuilder.group({
    id_region: [''],
    region: ['', Validators.required]
  });
  post_region = false;
  submitted = false;

  constructor(
    private region_service: RegionService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getRegions();
  }

  getRegions(){
    this.region_service.getRegions().subscribe(
      res => {
        this.regions = res;
        console.log(this.regions);
      },
      err => console.log(err)
    )
  }

  getRegion(id_region: number){
    this.region_service.getRegion(id_region).subscribe(
      res => {
        this.region = res;
        console.log(this.region);
      },
      err => console.log(err)
    )
  }

  onSubmit(){
    this.submitted = true;
    if(this.post_region){
      this.region_service.createRegion(this.formulario.value).subscribe(
        res => {
          console.log(this.region);
          this.getRegions();
          this.closeModal();
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'La region ha sido registrada',
            showConfirmButton: false,
            timer: 2000
          })
        },
        err => {
          console.log(err);
          Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'No se puede agregar esta region',
            showConfirmButton: false,
            timer: 2000
          })
        }
      )
    }else{
      this.region_service.updateRegion(this.formulario.value).subscribe(
        res => {
          console.log(this.region);
          this.getRegions();
          this.closeModal();
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'La region ha sido actualizada',
            showConfirmButton: false,
            timer: 2000
          })
        },
        err => {
          console.log(err);
          Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'No se puede actualizar esta region',
            showConfirmButton: false,
            timer: 2000
          })
        }
      )
    }
  }

  createRegion(){
    this.post_region = true;
    this.formulario.reset();
    $("#region_modal_create").modal("show");
  }

  updateRegion(region: Region){
    this.post_region = false;
    this.formulario.controls['id_region'].setValue(region.id_region);
    this.formulario.controls['region'].setValue(region.region);
    $("#region_modal_update").modal("show");
  }

  deleteRegion(id_region: number){
    Swal.fire({
      title: 'Esta seguro de eliminar la region?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.region_service.deleteRegion(id_region).subscribe(
          res => {
            console.log(this.region);
            this.getRegions();
          },
          err => {
            console.log(err);
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: 'La region seleccionada no puede ser eliminada',
              showConfirmButton: false,
              timer: 2000
            })
          }
        )
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'La region ha sido eliminada',
          showConfirmButton: false,
          timer: 2000
        })
      }
    })
  }

  get f() {
    return this.formulario.controls;
  }

  closeModal(){
    $("#region_modal_create").modal("hide");
    $("#region_modal_update").modal("hide");
    this.submitted = false;
  }
}
