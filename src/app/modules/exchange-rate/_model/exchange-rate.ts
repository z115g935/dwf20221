export class ExchangeRate{
    result!: string;
    documentation!: string;
    terms_of_use!: string;
    time_last_update_unix!: number;
    time_last_update_utc!: Date;
    time_next_update_unix!: number;
    time_next_update_utc!: Date;
    base_code!: string;
    conversion_rates!: Map<"rate", "value">;

    constructor(){
        this.result = "";
        this.documentation = "";
        this.terms_of_use = "";
        this.time_last_update_unix = 0;
        this.time_last_update_utc = new Date;
        this.time_next_update_unix = 0;
        this.time_next_update_utc = new Date;
        this.base_code = "";
        this.conversion_rates = new Map;
    }

}