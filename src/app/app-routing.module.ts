import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExchangeRateComponent } from './modules/exchange-rate/component/exchange-rate/exchange-rate.component';
import { RegionComponent } from './modules/customers/component/region/region.component';
import { HomeComponent } from './modules/home/component/home/home.component';
import { CategoryComponent } from './modules/product/component/category/category.component';
import { CustomerComponent } from './modules/customers/component/customer/customer.component';
import { CartComponent } from './modules/product/component/cart/cart.component';
import { FacturaComponent } from './modules/product/component/factura/factura.component';

const routes: Routes = [
  {path: 'exchange-rate', component: ExchangeRateComponent},
  {path: 'region', component: RegionComponent},
  {path: '', component: HomeComponent},
  {path: 'category', component: CategoryComponent},
  {path: 'clientes', component: CustomerComponent},
  {path: 'cart', component: CartComponent},
  {path: 'invoice', component: FacturaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
